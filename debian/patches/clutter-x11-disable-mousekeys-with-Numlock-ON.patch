From: Olivier Fourdan <ofourdan@redhat.com>
Date: Tue, 9 Apr 2019 16:25:57 +0200
Subject: clutter/x11: disable mousekeys with Numlock ON

GNOME documentation on accessibility features states that mousekeys
work only when NumLock is OFF:

  https://help.gnome.org/users/gnome-help/stable/mouse-mousekeys.html

Change the clutter/x11 implementation to match the documentation, i.e.
disable mousekeys when NumLock in ON so that switching NumLock ON
restores the numeric keypad behaviour.

Closes: https://gitlab.gnome.org/GNOME/mutter/issues/530
(cherry picked from commit 251fa024c416b1507a8cb1c9115082be87d25632)
---
 clutter/clutter/x11/clutter-backend-x11.c  | 20 ++++++++++++++++++++
 clutter/clutter/x11/clutter-xkb-a11y-x11.c |  9 +++++++--
 2 files changed, 27 insertions(+), 2 deletions(-)

diff --git a/clutter/clutter/x11/clutter-backend-x11.c b/clutter/clutter/x11/clutter-backend-x11.c
index 0ddd0f8..8c5ebc8 100644
--- a/clutter/clutter/x11/clutter-backend-x11.c
+++ b/clutter/clutter/x11/clutter-backend-x11.c
@@ -54,6 +54,7 @@
 #include "clutter-main.h"
 #include "clutter-private.h"
 #include "clutter-settings-private.h"
+#include "clutter-xkb-a11y-x11.h"
 
 G_DEFINE_TYPE (ClutterBackendX11, clutter_backend_x11, CLUTTER_TYPE_BACKEND)
 
@@ -276,6 +277,20 @@ clutter_backend_x11_create_device_manager (ClutterBackendX11 *backend_x11)
   _clutter_backend_add_event_translator (backend, translator);
 }
 
+static void
+on_keymap_state_change (ClutterKeymapX11 *keymap_x11,
+                        gpointer          data)
+{
+  ClutterDeviceManager *device_manager = CLUTTER_DEVICE_MANAGER (data);
+  ClutterKbdA11ySettings kbd_a11y_settings;
+
+  /* On keymaps state change, just reapply the current settings, it'll
+   * take care of enabling/disabling mousekeys based on NumLock state.
+   */
+  clutter_device_manager_get_kbd_a11y_settings (device_manager, &kbd_a11y_settings);
+  clutter_device_manager_x11_apply_kbd_a11y_settings (device_manager, &kbd_a11y_settings);
+}
+
 static void
 clutter_backend_x11_create_keymap (ClutterBackendX11 *backend_x11)
 {
@@ -292,6 +307,11 @@ clutter_backend_x11_create_keymap (ClutterBackendX11 *backend_x11)
       backend = CLUTTER_BACKEND (backend_x11);
       translator = CLUTTER_EVENT_TRANSLATOR (backend_x11->keymap);
       _clutter_backend_add_event_translator (backend, translator);
+
+      g_signal_connect (backend_x11->keymap,
+                        "state-changed",
+                        G_CALLBACK (on_keymap_state_change),
+                        backend->device_manager);
     }
 }
 
diff --git a/clutter/clutter/x11/clutter-xkb-a11y-x11.c b/clutter/clutter/x11/clutter-xkb-a11y-x11.c
index 6adde81..6b782c7 100644
--- a/clutter/clutter/x11/clutter-xkb-a11y-x11.c
+++ b/clutter/clutter/x11/clutter-xkb-a11y-x11.c
@@ -241,8 +241,13 @@ clutter_device_manager_x11_apply_kbd_a11y_settings (ClutterDeviceManager   *devi
     }
 
   /* mouse keys */
-  if (set_xkb_ctrl (desc, kbd_a11y_settings->controls,
-                    CLUTTER_A11Y_MOUSE_KEYS_ENABLED, XkbMouseKeysMask | XkbMouseKeysAccelMask))
+  if (clutter_keymap_get_num_lock_state (CLUTTER_KEYMAP (backend_x11->keymap)))
+    {
+      /* Disable mousekeys when NumLock is ON */
+      desc->ctrls->enabled_ctrls &= ~(XkbMouseKeysMask | XkbMouseKeysAccelMask);
+    }
+  else if (set_xkb_ctrl (desc, kbd_a11y_settings->controls,
+                         CLUTTER_A11Y_MOUSE_KEYS_ENABLED, XkbMouseKeysMask | XkbMouseKeysAccelMask))
     {
       gint mk_max_speed;
       gint mk_accel_time;
